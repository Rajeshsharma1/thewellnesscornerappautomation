import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import ch.qos.logback.core.joran.conditional.ElseAction as ElseAction
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.comment('Start Wellness store Application')

Mobile.startApplication('/Users/yogeshs/Downloads/Pharmacy build.apk', true)

WebUI.delay(150)

WebUI.comment('Tap on Enter Email')

Mobile.tap(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.widget.TextView - Enter your Email'), 10)

WebUI.delay(20)

WebUI.comment('Enter Email')

Mobile.setText(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.widget.EditText'), 'hiteshtest042@gmail.com', 
    10)

WebUI.delay(20)

WebUI.comment('Tap on Continue button')

Mobile.tap(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.widget.TextView - Continue'), 10)

WebUI.delay(20)

WebUI.comment('Enter Password')

Mobile.setText(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.widget.EditText (1)'), 'test@123', 10)

WebUI.delay(20)

WebUI.comment('Tap on Continue button')

Mobile.tap(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.widget.TextView - Continue (1)'), 10)

WebUI.delay(30)

WebUI.comment('Tap on Medicine and Pharmacy Store')

Mobile.tap(findTestObject('Object Repository/test/medicine/android.widget.TextView - Medicines  Pharmacy (1)'), 10)

WebUI.delay(20)

WebUI.comment('Tap on Location drop-down')

Mobile.tap(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.view.ViewGroup'), 10)

WebUI.delay(10)

WebUI.comment('Enter Chennai location on location search bar')

Mobile.setText(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.widget.EditText - Search your city'), 
    'Chennai', 10)

WebUI.delay(10)

WebUI.comment('Tap on Chennai location')

Mobile.tap(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.widget.TextView - Chennai'), 10)

WebUI.delay(10)

WebUI.comment('Verify Medicine title present')

if (Mobile.getText(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.widget.TextView - Medicines'), 10)) {
    System.out.println('Medicine Text are present')
} else {
    System.out.println('Test case fail')
}

WebUI.delay(20)

WebUI.comment('Tap on search medicine bar')

Mobile.tap(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.widget.TextView - Search medicines'), 10)

WebUI.delay(10)

WebUI.comment('Enter medicine name')

Mobile.setText(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.widget.EditText - Search Medicines'), 
    'alex', 10)

WebUI.delay(10)

WebUI.comment('Verify searched medicine shown')

if (Mobile.getText(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.widget.TextView - Alex p syrup'), 
    10)) {
    System.out.println('Medicine are present')
} else {
    System.out.println('Test case fail')
}

WebUI.delay(20)

WebUI.comment('Tap on searched medicine')

Mobile.tap(findTestObject('Object Repository/test/medicine/android.widget.TextView - Alex p syrup'), 10)

WebUI.delay(20)

WebUI.comment('verify searched medicne shown on medicine details screen')

if (Mobile.getText(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.widget.TextView - Alex P Syrup (1)'), 
    10)) {
    System.out.println('Test case pass')
} else {
    System.out.println('Test case fail')
}

WebUI.comment('Verify medicine alredy added in cart or not')

addtocart = Mobile.verifyElementExist(findTestObject('Object Repository/test/medicine/android.widget.TextView - Add to Cart'), 
    10, FailureHandling.OPTIONAL)

if (addtocart == true) {
    Mobile.tap(findTestObject('Object Repository/test/medicine/android.widget.TextView - Add to Cart'), 10, FailureHandling.OPTIONAL)
} else {
    Mobile.tap(findTestObject('Object Repository/test/New Pharmacy/camera photo select/medicine dertails View Cart'), 10)
}

WebUI.delay(20)

WebUI.comment('Tap on view cart')

Mobile.tap(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.widget.TextView -'), 10, FailureHandling.OPTIONAL)

WebUI.delay(10)

WebUI.comment('tap on view cart')

Mobile.tap(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.widget.TextView - View Cart'), 10, FailureHandling.OPTIONAL)

WebUI.delay(10)

Mobile.tap(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.widget.TextView - (1)'), 10, FailureHandling.OPTIONAL)

WebUI.delay(10)

Mobile.tap(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.widget.TextView - Alex P Syrup (2)'), 10)

WebUI.delay(10)

WebUI.comment('Tap on checkout')

Mobile.tap(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.widget.TextView - Checkout'), 10)

WebUI.delay(10)

WebUI.comment('Verify uploaded screen shown')

if (Mobile.getText(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.widget.TextView - Upload Prescription'), 
    10)) {
    System.out.println('Upload prescription screen present')
} else {
    System.out.println('Test case fail')
}

WebUI.delay(10)

WebUI.comment('Tap on prescription guide')

Mobile.tap(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.widget.TextView - View Prescription Guide'), 
    10)

WebUI.delay(10)

WebUI.comment('Remove prescription guide')

Mobile.tap(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.widget.TextView - Checkout'), 10)

WebUI.delay(10)

WebUI.comment('Tap on camera for prescription')

Mobile.tap(findTestObject('Object Repository/test/Medicine Pharmacy 2/a prescription Camera'), 10)

WebUI.delay(5)

WebUI.comment('Click on prescription photo')

Mobile.tap(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.view.V Tap on Camera'), 10)

WebUI.delay(10)

//Mobile.tap(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.widget.TextView - Gallery'), 10)
//
//WebUI.delay(10)
//
//Mobile.tap(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.widget.TextView - Camera'), 10)
//
//WebUI.delay(10)
//
//Mobile.tap(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.view.ViewGroup (1)'), 10)
//
//WebUI.delay(10)

WebUI.comment('Verify prescription attached')

if (Mobile.getText(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.widget.TextView - Attached Prescription'), 
    10)) {
    System.out.println('Prescription Atttached')
} else {
    System.out.println('Test case fail')
}

WebUI.delay(10)

WebUI.comment('Tap on checkout')

Mobile.tap(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.widget.TextView - Checkout'), 10)

WebUI.delay(10)

WebUI.comment('Verify select address screen present')

if (Mobile.getText(findTestObject('test/Medicine Pharmacy 2/medicine 2 Select Address'), 10, FailureHandling.OPTIONAL)) {
    System.out.println('Select Address screen present')
} else {
    System.out.println('Test case fail')
}

WebUI.delay(10)

WebUI.comment('Tap on Add new address')

Mobile.tap(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.widget.TextView -  Add New'), 10)

WebUI.delay(10)

WebUI.comment('Enter Pincode')

Mobile.setText(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.widget.EditText - Pincode'), '400002', 
    10)

WebUI.delay(10)

WebUI.comment('Enter location')

Mobile.setText(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.widget.EditText - Address'), 'Mumbai', 
    10)

WebUI.delay(10)

WebUI.comment('Enter Name')

Mobile.setText(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.widget.EditText - Name'), 'Hitesh', 10)

WebUI.delay(10)

WebUI.comment('Enter Phone number')

Mobile.setText(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.widget.EditText - Phone'), '9766338207', 
    10)

WebUI.delay(10)

WebUI.comment('Edit phone number')

Mobile.setText(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.widget.EditText - 9766338207'), '9766338207', 
    10)

WebUI.delay(10)

WebUI.comment('Tap on Home')

Mobile.tap(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.widget.TextView - HOME'), 10)

WebUI.delay(10)

WebUI.comment('Tap on Save button')

Mobile.tap(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.widget.TextView - SAVE'), 20)

WebUI.delay(30)

WebUI.comment('Tap on continue button')

Mobile.tap(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.widget.TextView - Continue (2)'), 10)

WebUI.delay(15)

WebUI.comment('Verify select seller screen present')

if (Mobile.getText(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.widget.TextView - Select Seller'), 
    10)) {
    System.out.println('Select seller screen present')
} else {
    System.out.println('Test case fail')
}

WebUI.delay(10)

WebUI.comment('Tap on view details')

Mobile.tap(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.widget.TextView - (2)'), 10)

WebUI.delay(10)

WebUI.comment('Verify added medicine on select seller screen')

if (Mobile.getText(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.widget.TextView - Alex P Syrup (3)'), 
    10)) {
    System.out.println('Added Medicine present')
} else {
    System.out.println('Test case fail')
}

WebUI.delay(10)

WebUI.comment('Tap on hide details')

Mobile.tap(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.widget.TextView - (3)'), 10)

WebUI.delay(10)

WebUI.comment('Verify same day delivery present')

if (Mobile.getText(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.widget.TextView - Same Day Delivery (only in selected cities)'), 
    10)) {
    System.out.println('Same day delivey shown')
} else {
    System.out.println('Test case fail')
}

WebUI.delay(10)

WebUI.comment('Tap on select seller radio button')

Mobile.tap(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.view.ViewGroup (2)'), 10)

WebUI.delay(10)

WebUI.comment('Tap on back button select address screen')

Mobile.tap(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.widget.TextView - (4)'), 10)

WebUI.delay(10)

WebUI.comment('Tap on continue button')

Mobile.tap(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.widget.TextView - Continue (2)'), 10)

WebUI.delay(10)

WebUI.comment('Again tap on select seller radio button')

Mobile.tap(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.view.ViewGroup (2)'), 10)

WebUI.delay(10)

WebUI.comment('tap on select seller confirm button')

Mobile.tap(findTestObject('test/Medicine Pharmacy 2/medicine 2  select seller Confirm'), 10)

WebUI.delay(10)

WebUI.comment('Verify order summary screen present')

if (Mobile.getText(findTestObject('test/Medicine Pharmacy 2/medicine 2 Order Summary'), 10)) {
    System.out.println('Order summary screen present')
} else {
    System.out.println('Test case fail')
}

WebUI.delay(10)

WebUI.comment('Tap on Item')

Mobile.tap(findTestObject('test/Medicine Pharmacy 2/Item drop down order summary TextView -'), 10)

WebUI.delay(20)

WebUI.comment('Tap on Apply promo code')

Mobile.tap(findTestObject('test/Medicine Pharmacy 2/Order summary Apply Promo Code'), 10)

WebUI.delay(20)

WebUI.comment('Tap on Apply promo code')

Mobile.tap(findTestObject('test/Medicine Pharmacy 2/Order summary Apply Promo Code'), 10)

WebUI.delay(20)

WebUI.comment('Enter promo code')

Mobile.setText(findTestObject('test/Medicine Pharmacy 2/a Enter Promo Code'), 'BUY50', 0)

Mobile.delay(5)

WebUI.comment('Tap on promo code apply')

Mobile.tap(findTestObject('test/Medicine Pharmacy 2/a Apply promo button'), 4)

Mobile.delay(5)

WebUI.comment('Tap on promo code apply')

Mobile.tap(findTestObject('test/Medicine Pharmacy 2/a Apply promo button'), 4, FailureHandling.OPTIONAL)

Mobile.delay(5)

WebUI.comment('Verify promo code applied')

Mobile.getText(findTestObject('test/Medicine Pharmacy 2/Promo Code Applied'), 10)

Mobile.delay(5)

WebUI.comment('Tap on change address')

Mobile.tap(findTestObject('test/Medicine Pharmacy 2/Change address Change'), 10)

WebUI.delay(20)

WebUI.comment('Tap on continue')

Mobile.tap(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.widget.TextView - Continue (2)'), 10)

WebUI.delay(20)

WebUI.comment('Tap on select seller radio button')

Mobile.tap(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.view.ViewGroup (2)'), 10)

WebUI.delay(20)

WebUI.comment('Tap on select seller confirm button')

Mobile.tap(findTestObject('test/Medicine Pharmacy 2/medicine 2  select seller Confirm'), 10)

WebUI.delay(20)

WebUI.comment('Tap continue button on order summary screen')

Mobile.tap(findTestObject('test/Medicine Pharmacy 2/order summary Continue'), 10)

Mobile.delay(5)

WebUI.comment('Tap on Ok I understand')

Mobile.tap(findTestObject('test/New Pharmacy/camera photo select/OK, I understand'), 10)

WebUI.delay(35)

WebUI.comment('Tap on Netbanking on RezerPay')

Mobile.tap(findTestObject('test/New Pharmacy/camera photo select/android.view.View - Netbanking All Indian banks'), 10)

WebUI.delay(5)

WebUI.comment('Select bank')

Mobile.tap(findTestObject('test/New Pharmacy/camera photo select/android.view.View - IDBI'), 10)

WebUI.delay(5)

WebUI.comment('Tap on Pay Now')

Mobile.tap(findTestObject('test/New Pharmacy/camera photo select/android.widget.Button - Pay Now'), 10)

WebUI.delay(10)

WebUI.comment('Tap on Success button')

Mobile.tap(findTestObject('test/New Pharmacy/camera photo select/android.widget.Button - Success'), 10)

WebUI.delay(20)

WebUI.comment('Verify payment done')

Mobile.getText(findTestObject('test/Medicine Pharmacy 2/Payment Done Thank you'), 10)

WebUI.delay(10)

WebUI.comment('Verify order ID')

Mobile.getText(findTestObject('test/Medicine Pharmacy 2/Order ID TWC-PHARMACY-1152-57CXP'), 10, FailureHandling.OPTIONAL)

WebUI.delay(10)

WebUI.comment('Tap on Track Order')

Mobile.tap(findTestObject('test/Medicine Pharmacy 2/Track Order'), 10)

WebUI.delay(10)

WebUI.comment('Verify order details screen present')

Mobile.getText(findTestObject('test/Medicine Pharmacy 2/a Order Details Screen'), 10)

WebUI.delay(10)

WebUI.comment('Close App')

Mobile.closeApplication()

