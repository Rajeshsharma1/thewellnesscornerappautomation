import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.logging.KeywordLogger as KeywordLogger
import com.kms.katalon.core.appium.driver.AppiumDriverManager as AppiumDriverManager
import io.appium.java_client.android.AndroidDriver as AndroidDriver
import org.openqa.selenium.remote.DesiredCapabilities as DesiredCapabilities
import com.kms.katalon.core.mobile.driver.MobileDriverType as MobileDriverType
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil

Mobile.startApplication('lt://APP10160532321713774378115658', false)

WebUI.comment('Tap on Medicine and Pharmacy Store')

not_run: CustomKeywords.'browserStack.appLaunch2.browserStack'()

KeywordLogger log = new KeywordLogger()

WebUI.delay(90)

username = Mobile.verifyElementExist(findTestObject('Hamburger menu'), 10, FailureHandling.OPTIONAL)

if (username == false) {
    WebUI.callTestCase(findTestCase('TestModules/call dashboard screen'), [:], FailureHandling.STOP_ON_FAILURE)
}

WebUI.delay(5)

dialog = Mobile.verifyElementExist(findTestObject('test/Medicine Pharmacy 2/Separating the best for you on dashboard'), 
    5, FailureHandling.OPTIONAL)

if (dialog == true) {
    Mobile.tap(findTestObject('test/Medicine Pharmacy 2/Maybe later on dashboard'), 10, FailureHandling.OPTIONAL)
}

WebUI.delay(10)

Mobile.tap(findTestObject('Object Repository/test/medicine/android.widget.TextView - Medicines  Pharmacy (1)'), 10)

WebUI.delay(15)

CustomKeywords.'browserStack.appLaunch.ErrorHandle'()

WebUI.comment('Tap on Location drop-down')

Mobile.tap(findTestObject('New folder/android.view.ViewGroup(location)'), 15)

WebUI.delay(7)

CustomKeywords.'browserStack.appLaunch.ErrorHandle'()

WebUI.comment('Enter Chennai location on location search bar')

Mobile.setText(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.widget.EditText - Search your city'), 
    'Chennai', 10)

WebUI.delay(6)

WebUI.comment('Tap on Chennai location')

Mobile.tap(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.widget.TextView - Chennai'), 10)

WebUI.comment('Verify Medicine title present')

medtitle = Mobile.verifyElementText(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.widget.TextView - Medicines'), 
    'Pharmacy', FailureHandling.OPTIONAL)

if (medtitle == true) {
    System.out.println('Medicine Text are present')
} else {
    System.out.println('Test case fail')

    not_run: assert false : 'Test case failed due to User is not at Medicines screen'
}

WebUI.delay(3)

WebUI.comment('Tap on search medicine bar')

Mobile.tap(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.widget.TextView - Search medicines'), 10)

WebUI.delay(3)

WebUI.comment('Enter medicine name')

Mobile.setText(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.widget.EditText - Search Medicines'), 
    'alex p syrup', 10)

WebUI.delay(13)

WebUI.comment('Verify searched medicine shown')

medtitle2 = Mobile.verifyElementText(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.widget.TextView - Alex p syrup'), 
    'Alex P Syrup', FailureHandling.OPTIONAL)

if (medtitle2 == true) {
    System.out.println('Medicine are present')
} else {
    System.out.println('Test case fail')

    assert false : 'Medicines search is not working.'
}

WebUI.delay(3)

WebUI.comment('Tap on searched medicine')

Mobile.tap(findTestObject('Object Repository/test/medicine/android.widget.TextView - Alex p syrup'), 10)

WebUI.delay(3)

WebUI.comment('verify searched medicne shown on medicine details screen')

CustomKeywords.'browserStack.appLaunch.ErrorHandle'()

medtitle3 = Mobile.verifyElementText(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.widget.TextView - Alex P Syrup (1)'), 
    'Alex P Syrup', FailureHandling.OPTIONAL)

if (medtitle3 == true) {
    System.out.println('Test case pass')
} else {
    System.out.println('Test case fail')
}

WebUI.comment('Verify medicine alredy added in cart or not')

addtocart = Mobile.verifyElementExist(findTestObject('Object Repository/test/medicine/android.widget.TextView - Add to Cart'), 
    10, FailureHandling.OPTIONAL)

if (addtocart == true) {
    Mobile.tap(findTestObject('Object Repository/test/medicine/android.widget.TextView - Add to Cart'), 10, FailureHandling.OPTIONAL)
}

CustomKeywords.'browserStack.appLaunch.outofstockhandle'()

Mobile.tap(findTestObject('Medicines Pharmacy with id elements/inner search on medicines details'), 10)

Mobile.delay(3)

Mobile.tap(findTestObject('Medicines Pharmacy with id elements/android.view.ViewGroup cross1'), 10)

Mobile.delay(3)

Mobile.setText(findTestObject('test/New Folder07/android.widget.EditText again- Search Medicines'), 'vogli', 10)

Mobile.delay(3)

Mobile.tap(findTestObject('test/New Folder07/android.widget.TextView - becosules capsule'), 10)

Mobile.delay(3)

CustomKeywords.'browserStack.appLaunch.ErrorHandle'()

addtocart = Mobile.verifyElementExist(findTestObject('Object Repository/test/medicine/android.widget.TextView - Add to Cart'), 
    10, FailureHandling.OPTIONAL)

if (addtocart == true) {
    Mobile.tap(findTestObject('Object Repository/test/medicine/android.widget.TextView - Add to Cart'), 10, FailureHandling.OPTIONAL)
}

CustomKeywords.'browserStack.appLaunch.outofstockhandle'()

Mobile.delay(3)

Mobile.pressBack()

Mobile.tap(findTestObject('Medicines Pharmacy with id elements/android.view.ViewGroup cross1'), 2, FailureHandling.OPTIONAL)

Mobile.delay(3)

Mobile.tap(findTestObject('Medicines Pharmacy with id elements/inner search on medicines details'), 10, FailureHandling.OPTIONAL)

Mobile.setText(findTestObject('test/New Folder07/android.widget.EditText again- Search Medicines'), 'nutrihale syrup', 10)

Mobile.delay(3)

Mobile.tap(findTestObject('test/New Folder07/android.widget.TextView - d-rise 60k capsule'), 10)

Mobile.delay(3)

CustomKeywords.'browserStack.appLaunch.ErrorHandle'()

CustomKeywords.'browserStack.appLaunch.outofstockhandle'()

addtocart = Mobile.verifyElementExist(findTestObject('Object Repository/test/medicine/android.widget.TextView - Add to Cart'), 
    10, FailureHandling.OPTIONAL)

if (addtocart == true) {
    Mobile.tap(findTestObject('Object Repository/test/medicine/android.widget.TextView - Add to Cart'), 10, FailureHandling.OPTIONAL)
} else {
    Mobile.tap(findTestObject('Object Repository/test/New Pharmacy/camera photo select/medicine dertails View Cart'), 10)
}

WebUI.delay(3)

WebUI.comment('Tap on view cart')

Mobile.tap(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.widget.TextView -'), 10, FailureHandling.OPTIONAL)

WebUI.delay(3)

WebUI.comment('tap on view cart')

Mobile.tap(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.widget.TextView - View Cart'), 10, FailureHandling.OPTIONAL)

WebUI.delay(3)

CustomKeywords.'browserStack.appLaunch.ErrorHandle'()

Mobile.tap(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.widget.TextView - (1)'), 10, FailureHandling.OPTIONAL)

WebUI.delay(3)

Mobile.scrollToText('Alex P Syrup', FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.widget.TextView - Alex P Syrup (2)'), 10)

WebUI.delay(3)

WebUI.comment('Tap on checkout')

Mobile.tap(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.widget.TextView - Checkout'), 10)

WebUI.delay(3)

WebUI.comment('Verify uploaded screen shown')

uploadpres = Mobile.verifyElementText(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.widget.TextView - Upload Prescription'), 
    'Upload Prescription', FailureHandling.OPTIONAL)

if (uploadpres == true) {
    System.out.println('Upload prescription screen present')
} else {
    System.out.println('Test case fail')

    assert false : 'User is not at Upload Prescription screen'
}

WebUI.delay(3)

WebUI.comment('Tap on prescription guide')

Mobile.tap(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.widget.TextView - View Prescription Guide'), 
    10)

WebUI.delay(3)

WebUI.comment('Remove prescription guide')

Mobile.tap(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.widget.TextView - Checkout'), 10)

WebUI.delay(3)

WebUI.comment('Tap on camera for prescription')

Mobile.tap(findTestObject('Object Repository/test/Medicine Pharmacy 2/a prescription Camera'), 10)

WebUI.delay(3)

WebUI.comment('Click on prescription photo')

Mobile.tap(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.view.V Tap on Camera'), 10)

WebUI.delay(3)

//Mobile.tap(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.widget.TextView - Gallery'), 10)
//
//WebUI.delay(3)
//
//Mobile.tap(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.widget.TextView - Camera'), 10)
//
//WebUI.delay(3)
//
//Mobile.tap(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.view.ViewGroup (1)'), 10)
//
//WebUI.delay(3)
WebUI.comment('Verify prescription attached')

prescriptionattach = Mobile.verifyElementText(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.widget.TextView - Attached Prescription'), 
    'Attached Prescription', FailureHandling.OPTIONAL)

if (prescriptionattach == true) {
    System.out.println('Prescription Atttached')
} else {
    System.out.println('Test case fail')
}

WebUI.delay(3)

WebUI.comment('Tap on checkout')

Mobile.tap(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.widget.TextView - Checkout'), 10)

WebUI.delay(3)

WebUI.comment('Verify select address screen present')

selectaddress = Mobile.verifyElementExist(findTestObject('test/Medicine Pharmacy 2/medicine 2 Select Address'), 10, FailureHandling.OPTIONAL)

if (selectaddress == true) {
    System.out.println('Select Address screen present')
} else {
    System.out.println('Test case fail')
}

WebUI.delay(3)

WebUI.comment('Tap on Add new address')

Mobile.tap(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.widget.TextView -  Add New'), 10)

WebUI.delay(3)

WebUI.comment('Enter Pincode')

Mobile.setText(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.widget.EditText - Pincode'), '400002', 
    10)

WebUI.delay(3)

WebUI.comment('Enter location')

Mobile.setText(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.widget.EditText - Address'), 'Mumbai', 
    10)

WebUI.delay(3)

WebUI.comment('Enter Name')

Mobile.setText(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.widget.EditText - Name'), 'Hitesh', 10)

WebUI.delay(3)

WebUI.comment('Enter Phone number')

Mobile.setText(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.widget.EditText - Phone'), '9766338207', 
    10)

WebUI.delay(3)

WebUI.comment('Edit phone number')

Mobile.setText(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.widget.EditText - 9766338207'), '9766338207', 
    10)

WebUI.delay(3)

WebUI.comment('Tap on Home')

Mobile.tap(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.widget.TextView - HOME'), 10)

WebUI.delay(3)

WebUI.comment('Tap on Save button')

Mobile.tap(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.widget.TextView - SAVE'), 20)

WebUI.delay(3)

WebUI.comment('Tap on continue button')

Mobile.tap(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.widget.TextView - Continue (2)'), 10)

WebUI.delay(8)

WebUI.comment('Verify select seller screen present')

CustomKeywords.'browserStack.appLaunch.ErrorHandle'()

WebUI.delay(3)

selectseller = Mobile.verifyElementExist(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.widget.TextView - Select Seller'), 
    10)

if (selectseller == true) {
    System.out.println('Select seller screen present')
} else {
    System.out.println('Test case fail')
}

WebUI.delay(3)

WebUI.comment('Tap on view details')

Mobile.tap(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.widget.TextView - (2)'), 10)

WebUI.delay(3)

WebUI.comment('Verify added medicine on select seller screen')

medtitle4 = Mobile.verifyElementText(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.widget.TextView - Alex P Syrup (3)'), 
    'Alex P Syrup', FailureHandling.OPTIONAL)

if (medtitle4 == true) {
    System.out.println('Added Medicine present')
} else {
    System.out.println('Test case fail')
}

WebUI.delay(3)

WebUI.comment('Tap on hide details')

Mobile.tap(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.widget.TextView - (3)'), 10)

WebUI.delay(3)

WebUI.comment('Verify same day delivery present')

deliverypresent = Mobile.verifyElementExist(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.widget.TextView - Same Day Delivery (only in selected cities)'), 
    10, FailureHandling.OPTIONAL)

if (deliverypresent == true) {
    System.out.println('Same day delivey shown')
} else {
    System.out.println('Test case fail')
}

WebUI.delay(3)

WebUI.comment('Tap on select seller radio button')

Mobile.tap(findTestObject('Medicines Pharmacy with id elements/radio button on select address'), 10)

WebUI.delay(3)

WebUI.comment('Tap on back button select address screen')

Mobile.tap(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.widget.TextView - (4)'), 10)

WebUI.delay(3)

WebUI.comment('Tap on continue button')

Mobile.tap(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.widget.TextView - Continue (2)'), 10)

WebUI.delay(3)

WebUI.comment('Again tap on select seller radio button')

Mobile.tap(findTestObject('Medicines Pharmacy with id elements/radio button on select address'), 10)

WebUI.delay(3)

WebUI.comment('tap on select seller confirm button')

Mobile.tap(findTestObject('test/Medicine Pharmacy 2/medicine 2  select seller Confirm'), 10)

WebUI.delay(8)

unavailableMedicines = Mobile.verifyElementVisible(findTestObject('test/Medicine Pharmacy 2/Continue to checkout with available medicines'), 
    10, FailureHandling.OPTIONAL)

if (unavailableMedicines == true) {
    Mobile.tap(findTestObject('test/Medicine Pharmacy 2/a checkout _ Continue'), 10, FailureHandling.OPTIONAL)
}

WebUI.delay(8)

WebUI.comment('Verify order summary screen present')

ordersummery = Mobile.verifyElementExist(findTestObject('test/Medicine Pharmacy 2/medicine 2 Order Summary'), 10, FailureHandling.OPTIONAL)

if (ordersummery == true) {
    System.out.println('Order summary screen present')
} else {
    System.out.println('Test case fail')
}

WebUI.comment('Tap on Item')

Mobile.tap(findTestObject('test/Medicine Pharmacy 2/android.widget.Tap on Item drop down'), 10, FailureHandling.OPTIONAL)

WebUI.delay(5)

WebUI.comment('Press Back')

Mobile.pressBack()

//test07 = WebUI.verifyTextPresent('Promo Code Applied', true)
//
//if (test07 == true) {
//    System.out.println('Promo code AutoApplied')
//
//    Mobile.delay(3)
//
//    WebUI.comment('Tap on continue')
//
//    Mobile.tap(findTestObject('test/Medicine Pharmacy 2/order summary Continue'), 10, FailureHandling.STOP_ON_FAILURE)
//} else {
//    WebUI.delay(3)
//
//    WebUI.comment('Tap on Apply promo code')
//
//    Mobile.tap(findTestObject('test/New Folder07/android.w.TextView - Apply Promo Code2'), 10)
//
//    WebUI.delay(3)
//
//    WebUI.comment('Enter promo code')
//
//    Mobile.setText(findTestObject('test/New Folder07/android.widget.EditText - Enter Promo Code2'), 'TRUWORTH1', 10, FailureHandling.OPTIONAL)
//
//    Mobile.delay(3)
//
//    Mobile.tap(findTestObject('test/New Folder07/android.widget.TextView - Apply_BUTTON2'), 4, FailureHandling.OPTIONAL)
//
//    Mobile.delay(3)
//
//    WebUI.comment('Verify promo code applied')
//
//    Mobile.verifyElementText(findTestObject('Object Repository/test/New Folder07/android.widget.TextView - Promo Code Applied2'), 
//        'Promo Code Applied', FailureHandling.OPTIONAL)
//
//    Mobile.pressBack()
//}
//
//Mobile.delay(3)
WebUI.comment('Tap on continue')

Mobile.tap(findTestObject('test/Medicine Pharmacy 2/order summary Continue'), 10, FailureHandling.OPTIONAL)

Mobile.delay(3)

WebUI.comment('Tap on Ok I understand')

Mobile.tap(findTestObject('test/New Pharmacy/camera photo select/OK, I understand'), 10, FailureHandling.OPTIONAL)

WebUI.delay(15)

CustomKeywords.'browserStack.appLaunch.ErrorHandle'()

WebUI.comment('Tap on Netbanking on RezerPay')

production2 = Mobile.verifyElementVisible(findTestObject('test/New Pharmacy/camera photo select/android.view.View - Netbanking All Indian banks'), 
    15)

if (production2 == true) {
    Mobile.tap(findTestObject('test/New Pharmacy/camera photo select/android.view.View - Netbanking All Indian banks'), 
        15)
} else {
    System.out.println('Test case fail due to we test on production')
}

WebUI.delay(7)

WebUI.comment('Select bank')

Mobile.tap(findTestObject('test/Medicine Pharmacy 2/IDBI bank via id'), 10, FailureHandling.OPTIONAL)

WebUI.comment('Select bank')

Mobile.tap(findTestObject('test/Medicine Pharmacy 2/android.view.View - IDBI'), 10, FailureHandling.OPTIONAL)

WebUI.delay(5)

WebUI.comment('Tap on Pay Now')

Mobile.tap(findTestObject('test/New Pharmacy/camera photo select/android.widget.Button - Pay Now'), 10)

WebUI.delay(10)

WebUI.comment('Tap on Success button')

Mobile.tap(findTestObject('test/New Pharmacy/camera photo select/android.widget.Button - Success'), 10)

WebUI.delay(10)

WebUI.comment('Verify payment done')

thankyouscreen = Mobile.verifyElementVisible(findTestObject('test/Medicine Pharmacy 2/Payment Done Thank you'), 10, FailureHandling.OPTIONAL)

if (thankyouscreen == true) {
    WebUI.comment('Verify payment done')
} else {
    WebUI.comment('Payment is not done')

    assert false : 'Test case failed- Order is not created.'
}

WebUI.delay(5)

WebUI.comment('Verify order ID')

orderID = Mobile.verifyElementVisible(findTestObject('test/Medicine Pharmacy 2/Order ID TWC-PHARMACY-1152-57CXP'), 10, FailureHandling.OPTIONAL)

if (orderID == true) {
    WebUI.comment('Verify order ID')
} else {
    WebUI.comment('Order ID is not varified.')
}

WebUI.delay(5)

WebUI.comment('Tap on Track Order')

Mobile.tap(findTestObject('test/Medicine Pharmacy 2/Track Order'), 10)

WebUI.delay(10)

WebUI.comment('Verify order details screen present')

orderDetail = Mobile.verifyElementVisible(findTestObject('test/Medicine Pharmacy 2/a Order Details Screen'), 10, FailureHandling.OPTIONAL)

if (orderDetail == true) {
    WebUI.comment('Verify order details screen present')
} else {
    WebUI.comment('Verify order details screen is not present')

    assert false : 'Test case failed- Order not created.'
}

WebUI.delay(3)

medverify = Mobile.verifyElementText(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.widget.TextView - Alex P Syrup (3)'), 
    'Alex P Syrup', FailureHandling.OPTIONAL)

if (medverify == true) {
    System.out.println('Added Medicine present')
} else {
    System.out.println('Test case fail')
}

WebUI.comment('Close App')

Mobile.closeApplication()

