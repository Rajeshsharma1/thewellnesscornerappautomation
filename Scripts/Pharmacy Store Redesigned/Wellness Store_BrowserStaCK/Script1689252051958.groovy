import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.logging.KeywordLogger as KeywordLogger
import com.kms.katalon.core.appium.driver.AppiumDriverManager as AppiumDriverManager
import io.appium.java_client.android.AndroidDriver as AndroidDriver
import org.openqa.selenium.remote.DesiredCapabilities as DesiredCapabilities
import com.kms.katalon.core.mobile.driver.MobileDriverType as MobileDriverType
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil

WebUI.comment('Tap on Wellness Store')

CustomKeywords.'browserStack.appLaunch.browserStack'()

KeywordLogger log = new KeywordLogger()

WebUI.delay(8)

username = Mobile.verifyElementExist(findTestObject('Hamburger menu'), 10, FailureHandling.OPTIONAL)

if (username == false) {
    WebUI.callTestCase(findTestCase('TestModules/call dashboard screen'), [:], FailureHandling.STOP_ON_FAILURE)
}

WebUI.delay(5)

dialog = Mobile.verifyElementExist(findTestObject('test/Medicine Pharmacy 2/Separating the best for you on dashboard'), 
    5, FailureHandling.OPTIONAL)

if (dialog == true) {
    Mobile.tap(findTestObject('test/Medicine Pharmacy 2/Maybe later on dashboard'), 10, FailureHandling.OPTIONAL)
}

WebUI.delay(3)

Mobile.tap(findTestObject('Object Repository/test/Wellnesss Store/Wellness Store'), 10)

WebUI.delay(20)

CustomKeywords.'browserStack.appLaunch.ErrorHandle'()

WebUI.comment('Tap on Location drop-down')

Mobile.tap(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.view.ViewGroup'), 15)

WebUI.delay(10)

CustomKeywords.'browserStack.appLaunch.ErrorHandle'()

WebUI.comment('Enter Chennai location on location search bar')

Mobile.setText(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.widget.EditText - Search your city'), 
    'Chennai', 10)

WebUI.delay(3)

WebUI.comment('Tap on Chennai location')

Mobile.tap(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.widget.TextView - Chennai'), 10)

WebUI.delay(3)

WebUI.comment('Tap on again Location drop-down')

Mobile.tap(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.view.ViewGroup'), 15)

WebUI.delay(10)

WebUI.comment('Tap on detect location')

//Mobile.tap(findTestObject('Object Repository/test/Wellness_Store2/android.view.ViewGroup'), 10)
WebUI.delay(3)

WebUI.comment('Tap on detect location')

Mobile.tap(findTestObject('Medicines Pharmacy with id elements/android.view.ViewGroup detect location 4'), 5)

WebUI.delay(3)

WebUI.comment('Verify Wellness Store title present')

WebUI.delay(3)

//Mobile.tap(findTestObject('Medicines Pharmacy with id elements/android.widget.TextView - Tata 1mg Health ProductsTap 1'), 
//    3)
//
//WebUI.delay(3)
//
//Mobile.tap(findTestObject('Medicines Pharmacy with id elements/android.widget.TextView -search icon well 2'), 3)
//
//WebUI.delay(3)
//
//WebUI.setText(findTestObject('Medicines Pharmacy with id elements/android.widget.EditText - Search Health Products7'), 'tata 1mg salmon')
//
//WebUI.delay(3)
//
//Mobile.tap(findTestObject('Medicines Pharmacy with id elements/android.widget.TextView - Tata 1mg Salmon Omega 3 Fish Oil CapsuleTap 4'), 
//    3)
//
//WebUI.delay(3)
//
//WebUI.comment('Verify medicine alredy added in cart or not')
//
//addtocart7 = Mobile.verifyElementExist(findTestObject('Object Repository/test/medicine/android.widget.TextView - Add to Cart'), 
//    10, FailureHandling.OPTIONAL)
//
//if (addtocart7 == true) {
//    Mobile.tap(findTestObject('Object Repository/test/medicine/android.widget.TextView - Add to Cart'), 10, FailureHandling.OPTIONAL)
//}

Mobile.tap(findTestObject('test/New Wellness/a Tap- Search Health Products'), 10)

WebUI.delay(3)

Mobile.setText(findTestObject('test/New Wellness/a Set_Text - Search Health Products'), 'mamaearth', 10)

WebUI.delay(3)

Mobile.tap(findTestObject('Medicines Pharmacy with id elements/android.widget.TextView - MamaearthNEW'), 10)

WebUI.delay(3)

Mobile.tap(findTestObject('Newww Wellness/Relevant List Add to Cart'), 5)

WebUI.delay(3)

Mobile.tap(findTestObject('Newww Wellness/aDetails Page- Mamaearth Onion Conditioner'), 5)

WebUI.delay(3)

Mobile.verifyElementExist(findTestObject('Newww Wellness/aVerify Details Page  - Mamaearth Onion Conditioner'), 10)

WebUI.delay(3)

Mobile.tap(findTestObject('Newww Wellness/Add to cart symbol'), 10)

WebUI.delay(3)

//Mobile.tap(findTestObject('test/New Wellness/a Tap_Patanjali Ayurveda Coconut Oil'), 10)
//
//WebUI.delay(3)
//
//CustomKeywords.'browserStack.appLaunch.ErrorHandle'()
//
//WebUI.delay(3)
//
//WebUI.comment('Verify searched product shown')
//
//Mobile.getText(findTestObject('test/New Wellness/a Get- Patanjali Ayurveda Coconut Oil'), 10)
//
//WebUI.delay(3)
WebUI.comment('Verify medicine alredy added in cart or not')

addtocart4 = Mobile.verifyElementExist(findTestObject('Object Repository/test/medicine/android.widget.TextView - Add to Cart'), 
    10, FailureHandling.OPTIONAL)

if (addtocart4 == true) {
    Mobile.tap(findTestObject('Object Repository/test/medicine/android.widget.TextView - Add to Cart'), 10, FailureHandling.OPTIONAL)
}

Mobile.delay(3)

CustomKeywords.'browserStack.appLaunch.ErrorHandle'()

//WebUI.comment('Tap on Inner search')
//
//Mobile.tap(findTestObject('Medicines Pharmacy with id elements/inner search on medicines details'), 10)
//
//Mobile.delay(3)
//
//WebUI.comment('Tap on cross clear medicine')
//
//Mobile.tap(findTestObject('Object Repository/test/Wellness_Store2/android.view.ViewGroup (1)'), 10)
//
//Mobile.delay(3)
//
//WebUI.comment('Enter dabur health product')
//
//Mobile.setText(findTestObject('Object Repository/test/Wellness_Store2/android.widget.EditText - Search Health Products'), 
//    'dabur', 10)
//
//Mobile.delay(3)
//
//WebUI.comment('Tap on dabur health product')
//
//Mobile.tap(findTestObject('Object Repository/test/Wellness_Store2/android.widget.TextView - Dabur Honitus Herbal Cough Remedy'), 
//    10)
//
//Mobile.delay(3)
//
//CustomKeywords.'browserStack.appLaunch.ErrorHandle'()
//
//Mobile.delay(3)
//
//WebUI.comment('Verify added health product')
//
//Mobile.getText(findTestObject('Object Repository/test/Wellness_Store2/android.widget.TextView - Dabur Honitus Herbal Cough Remedy (1)'), 
//    10)
//
//addtocart4 = Mobile.verifyElementExist(findTestObject('Object Repository/test/medicine/android.widget.TextView - Add to Cart'), 
//    10, FailureHandling.OPTIONAL)
//
//if (addtocart4 == true) {
//    Mobile.tap(findTestObject('Object Repository/test/medicine/android.widget.TextView - Add to Cart'), 10, FailureHandling.OPTIONAL)
//
//    Mobile.delay(3)
//
//    Mobile.tap(findTestObject('Object Repository/test/New Pharmacy/camera photo select/medicine dertails View Cart'), 10, 
//        FailureHandling.OPTIONAL)
//} else {
//    Mobile.tap(findTestObject('Object Repository/test/New Pharmacy/camera photo select/medicine dertails View Cart'), 10)
//}
//
//Mobile.delay(3)
//
//CustomKeywords.'browserStack.appLaunch.ErrorHandle'()
//
//Mobile.delay(3)
WebUI.comment('Verify Cart Screen')

Cartscreen = Mobile.verifyElementText(findTestObject('Object Repository/test/Wellness_Store2/android.widget.TextView - Cart'), 
    'Cart', FailureHandling.OPTIONAL)

if (Cartscreen == true) {
    System.out.println('Cart screen shown')
} else {
    System.out.println('Test case fail')

    assert false : 'Cart screen not show'
}

//Mobile.getText(findTestObject('Object Repository/test/Wellness_Store2/android.widget.TextView - Cart'), 10)
Mobile.delay(3)

WebUI.comment('Quantity added(+)')

Mobile.tap(findTestObject('Object Repository/test/Wellness_Store2/android.widget.TextView - (1)'), 10, FailureHandling.OPTIONAL)

Mobile.delay(3)

WebUI.comment('Tap on Checkout')

Mobile.tap(findTestObject('Object Repository/test/Wellness_Store2/android.widget.TextView - Checkout'), 10)

Mobile.delay(3)

WebUI.comment('Verify Select address screen')

SelectAddress = Mobile.verifyElementText(findTestObject('Object Repository/test/Wellness_Store2/android.widget.TextView - Select Address'), 
    'Select Address', FailureHandling.OPTIONAL)

if (SelectAddress == true) {
    System.out.println('Select Address screen shown')
} else {
    System.out.println('Test case fail')

    assert false : 'Select address screen not shown'
}

//Mobile.getText(findTestObject('Object Repository/test/Wellness_Store2/android.widget.TextView - Select Address'), 10)
Mobile.delay(3)

WebUI.comment('Select address screen')

WebUI.comment('Tap on Add new address')

Mobile.tap(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.widget.TextView -  Add New'), 10)

WebUI.delay(3)

WebUI.comment('Enter Pincode')

Mobile.setText(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.widget.EditText - Pincode'), '400002', 
    10)

WebUI.delay(3)

WebUI.comment('Enter location')

Mobile.setText(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.widget.EditText - Address'), 'Mumbai', 
    10)

WebUI.delay(3)

WebUI.comment('Enter Name')

Mobile.setText(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.widget.EditText - Name'), 'Hitesh', 10)

WebUI.delay(3)

WebUI.comment('Enter Phone number')

Mobile.setText(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.widget.EditText - Phone'), '9766338207', 
    10)

WebUI.delay(3)

WebUI.comment('Edit phone number')

Mobile.setText(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.widget.EditText - 9766338207'), '9766338207', 
    10)

WebUI.delay(3)

WebUI.comment('Tap on Home')

Mobile.tap(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.widget.TextView - HOME'), 10)

WebUI.delay(3)

WebUI.comment('Tap on Save button')

Mobile.tap(findTestObject('Object Repository/test/Medicine Pharmacy 2/android.widget.TextView - SAVE'), 20)

WebUI.delay(3)

//Mobile.tap(findTestObject('Object Repository/test/Wellness_Store2/android.widget.TextView - jaipurMumbai, Maharashtra 400001Mobile 91 7975754946'), 
//10)
//Mobile.delay(3)
WebUI.comment('Tap on Continue')

Mobile.tap(findTestObject('Object Repository/test/Wellness_Store2/android.widget.TextView - Continue'), 10)

Mobile.delay(3)

WebUI.comment('Verify confirm order pop-up')

ConfirmOrder = Mobile.verifyElementText(findTestObject('Object Repository/test/Wellness_Store2/android.widget.TextView - Confirm Order'), 
    'Confirm Order', FailureHandling.OPTIONAL)

if (ConfirmOrder == true) {
    System.out.println('Confirm order pop-up shown')
} else {
    System.out.println('Test case fail')

    assert false : 'Order not confirm'
}

//Mobile.getText(findTestObject('Object Repository/test/Wellness_Store2/android.widget.TextView - Confirm Order'), 10)
Mobile.delay(3)

WebUI.comment('Tap on continue')

Mobile.tap(findTestObject('Object Repository/test/Wellness_Store2/android.widget.TextView - Continue (1)'), 10)

Mobile.delay(3)

WebUI.comment('Verify order summary screen')

OrderSummary = Mobile.verifyElementText(findTestObject('Object Repository/test/Wellness_Store2/android.widget.TextView - Order Summary'), 
    'Order Summary', FailureHandling.OPTIONAL)

if (OrderSummary == true) {
    System.out.println('Order summary screen present')
} else {
    System.out.println('Test case fail')

    assert false : 'Order summary screen not present'
}

//Mobile.getText(findTestObject('Object Repository/test/Wellness_Store2/android.widget.TextView - Order Summary'), 10)
Mobile.delay(3)

WebUI.comment('Tap on Item cart')

Mobile.tap(findTestObject('Medicines Pharmacy with id elements/android.view.ViewGroupitem card side bar'), 10)

Mobile.delay(3)

WebUI.comment('Tap on Item cart remove')

Mobile.tap(findTestObject('Object Repository/test/Wellness_Store2/android.widget.TextView - Order Summary (1)'), 10)

Mobile.delay(3)

//
//WebUI.comment('Tap on apply promo code')
//
//Mobile.tap(findTestObject('Object Repository/test/Wellness_Store2/android.widget.TextView - Apply Promo Code'), 10)
//
//Mobile.delay(3)
//
//WebUI.comment('Enter Promo code')
//
//Mobile.setText(findTestObject('Object Repository/test/Wellness_Store2/android.widget.EditText - Enter Promo Code (1)'), 
//    'ALEX50', 10)
//
//Mobile.delay(3)
//
//WebUI.comment('Tap on apply')
//
//Mobile.tap(findTestObject('Object Repository/test/Wellness_Store2/android.widget.TextView - Apply'), 10)
//
//Mobile.delay(3)
//
//WebUI.comment('Verify Promo code applied')
//
//PromoCode = Mobile.verifyElementText(findTestObject('Object Repository/test/Wellness_Store2/android.widget.TextView - Promo Code Applied'), 
//    'Promo Code Applied', FailureHandling.OPTIONAL)
//
//if (PromoCode == true) {
//    System.out.println('Verify Promo code applied')
//} else {
//    System.out.println('Test case fail')
//
//    assert false : 'Promo code not aaplied'
//}
//Mobile.getText(findTestObject('Object Repository/test/Wellness_Store2/android.widget.TextView - Promo Code Applied'), 10)
//Mobile.delay(3)
WebUI.comment('Tap on continue')

Mobile.tap(findTestObject('Object Repository/test/Wellness_Store2/android.widget.TextView - Continue (2)'), 10)

Mobile.delay(3)

WebUI.comment('Verify Disclamier Pop-up')

Disclamier = Mobile.verifyElementText(findTestObject('Object Repository/test/Wellness_Store2/android.widget.TextView - Disclaimer'), 
    'Disclaimer', FailureHandling.OPTIONAL)

if (Disclamier == true) {
    System.out.println('Disclamier pop-up shown')
} else {
    System.out.println('Test case fail')

    assert false : 'Disclamier pop-up not shown'
}

//Mobile.getText(findTestObject('Object Repository/test/Wellness_Store2/android.widget.TextView - Disclaimer'), 10)
Mobile.delay(3)

WebUI.comment('Tap on OK I Understand')

Mobile.tap(findTestObject('Object Repository/test/Wellness_Store2/android.widget.TextView -  OK, I understand'), 10)

WebUI.delay(7)

CustomKeywords.'browserStack.appLaunch.ErrorHandle'()

Mobile.delay(15)

WebUI.comment('Tap on Netbanking on RezerPay')

Production = Mobile.verifyElementVisible(findTestObject('test/New Pharmacy/camera photo select/android.view.View - Netbanking All Indian banks'), 15)

if(Production == true ) {
	
	Mobile.tap(findTestObject('test/New Pharmacy/camera photo select/android.view.View - Netbanking All Indian banks'), 15)
	
}

else 
{
	System.out.println("Test case fail due to we test on production")
}
WebUI.delay(7)

WebUI.comment('Select bank')

Mobile.tap(findTestObject('test/Medicine Pharmacy 2/IDBI bank via id'), 10, FailureHandling.OPTIONAL)

WebUI.comment('Select bank')

Mobile.tap(findTestObject('test/Medicine Pharmacy 2/android.view.View - IDBI'), 10, FailureHandling.OPTIONAL)

WebUI.delay(5)

WebUI.comment('Tap on Pay Now')

Mobile.tap(findTestObject('test/New Pharmacy/camera photo select/android.widget.Button - Pay Now'), 10)

WebUI.delay(10)

WebUI.comment('Tap on Success button')

Mobile.tap(findTestObject('test/New Pharmacy/camera photo select/android.widget.Button - Success'), 10)

WebUI.delay(10)

WebUI.comment('Verify payment done')

thankyouscreen = Mobile.verifyElementVisible(findTestObject('test/Medicine Pharmacy 2/Payment Done Thank you'), 10, FailureHandling.OPTIONAL)

if (thankyouscreen == true) {
    WebUI.comment('Verify payment done')
} else {
    WebUI.comment('Payment is not done')

    assert false : 'Test case failed- Order is not created.'
}

WebUI.delay(5)

WebUI.comment('Tap on Track Order')

Mobile.tap(findTestObject('test/Medicine Pharmacy 2/Track Order'), 10)

WebUI.delay(10)

WebUI.comment('Verify order details screen present')

orderDetail = Mobile.verifyElementVisible(findTestObject('test/Medicine Pharmacy 2/a Order Details Screen'), 10, FailureHandling.OPTIONAL)

if (orderDetail == true) {
    WebUI.comment('Verify order details screen present')
} else {
    WebUI.comment('Verify order details screen is not present')

    assert false : 'Test case failed- Order not created.'
}

WebUI.delay(3)

medverify = Mobile.verifyElementText(findTestObject('Object Repository/test/Wellness_Store2/android.widget.TextView - Patanjali Ayurveda Coconut Oil (1)'), 
    'Patanjali Ayurveda Coconut Oil', 10)

if (medverify == true) {
    System.out.println('Added Health product present')
} else {
    System.out.println('Test case fail')
}

WebUI.comment('Close App')

Mobile.closeApplication()

