package browserStack
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords
import internal.GlobalVariable
import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By
import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webui.exception.WebElementNotFoundException
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.logging.KeywordLogger as KeywordLogger
import com.kms.katalon.core.appium.driver.AppiumDriverManager as AppiumDriverManager
import io.appium.java_client.android.AndroidDriver as AndroidDriver
import org.openqa.selenium.remote.DesiredCapabilities as DesiredCapabilities
import com.kms.katalon.core.mobile.driver.MobileDriverType as MobileDriverType
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject


class appLaunch {
	@Keyword
	public static void browserStack() {

		WebUI.comment('Start Wellness store Application')
		String browserStackServerURL = 'https://rajeshsharma_7LnPjx:hgkXAULkWJMN9xCzWoJb@hub-cloud.browserstack.com/wd/hub'

		DesiredCapabilities capabilities = new DesiredCapabilities()

		capabilities.setCapability('device', 'Google Pixel 4')

		//Set the app_url (returned on uploading app on Browserstack) in the 'app' capability
		capabilities.setCapability('app', 'bs://02a97d9864cbcc74a5dfdad59d5b44fe197454d1')

		capabilities.setCapability('autoGrantPermissions', 'true')


		// set the timeout to a maximum of 300 seconds

		//capabilities.setCapability("browserstack.idleTimeout", "<time_in_seconds>")

		AppiumDriverManager.createMobileDriver(MobileDriverType.ANDROID_DRIVER, capabilities, new URL(browserStackServerURL))
	}

	@Keyword
	public static void ErrorHandle() {

		boolean alert = Mobile.verifyElementExist(findTestObject('Object Repository/test/Something went wrong dialog'), 10, FailureHandling.OPTIONAL)

		boolean alert2 = Mobile.verifyElementExist(findTestObject('Object Repository/test/Medicine Pharmacy 2/Something went wrong 2'), 10, FailureHandling.OPTIONAL)

		if (alert == true || alert2 == true) {
			Mobile.tap(findTestObject('Object Repository/test/OK on alert pop-up'), 5, FailureHandling.OPTIONAL)

			Mobile.tap(findTestObject('Object Repository/test/Medicine Pharmacy 2/OK 2'), 5, FailureHandling.OPTIONAL)

			assert false : 'Test case failed due to APIs not working now.'
		}
	}
	@Keyword
	public static void outofstockhandle() {

		boolean outofstock = Mobile.verifyElementExist(findTestObject('Object Repository/test/Something went wrong dialog'), 10, FailureHandling.OPTIONAL)

		boolean locationUnserviceble = Mobile.verifyElementExist(findTestObject('Object Repository/test/Something went wrong dialog'), 10, FailureHandling.OPTIONAL)

		if (outofstock == true || locationUnserviceble == true) {

			WebUI.comment('Searched item is OUT OF STOCK')
			//assert false : 'Searched item is OUT OF STOCK'
		}
	}
}